<?php


class eldes
{

    /**
     * @var int
     */
    public $cena=11123.23454;

    /**
     * @var string
     */
    public $mena = 'CZK';

    public function getPrizeVotypkaMartin()
    {
       $this->cena = number_format($this->cena, 2, ',', ' ');


        return $this->cena . ' ' . $this->mena;
    }

    public function zaokrouhliPrizeVotypkaMartin(){
        $this->cena = round(11123.234, 2);
        return $this->cena . ' ' . $this->mena;
    }

}